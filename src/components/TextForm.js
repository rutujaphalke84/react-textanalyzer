import React, {useState} from 'react'



export default function TextForm(props) {
    const handleUpClick = ()=>{
        //console.log("UpperCase was clicked" + text);
        let newText = text.toUpperCase();
        setText(newText)
    }

    const handleOnChange = (event)=>{
        //console.log("OnChange was clicked");
        setText(event.target.value);
    }

    const handleLwClick = ()=>{
        //console.log("LowerCase was clicked" + text);
        let newText = text.toLowerCase();
        setText(newText)
    }

    const handleClearClick = ()=>{
        //console.log("LowerCase was clicked" + text);
        let newText = '';
        setText(newText)
    }

    const handleExtraSpaces = ()=>{
        
        let newText = text.replace(/\s+/g,' ').trim();
        setText(newText)
    }

    const handleReverseText = ()=>{
        let newText = text.split("").reverse().join("");
        setText(newText)
    }

    const [text, setText] = useState('');

    return (
        <>
        <div className="container">
            <h1>{props.heading}</h1>
            <div className="mb-3">
                <textarea className="form-control" value = {text} onChange={handleOnChange} id="myBox" rows="8"></textarea>
            </div> 
            <button className="btn btn-primary mx-2" onClick={handleUpClick}>Convert to Uppercase</button>
            <button className="btn btn-primary mx-2" onClick={handleLwClick}>Convert to Lowercase</button>
            <button className="btn btn-primary mx-2" onClick={handleClearClick}>Clear text</button>
            <button className="btn btn-primary mx-2" onClick={handleExtraSpaces}>Remove Extra Spaces</button>
            <button className="btn btn-primary mx-2" onClick={handleReverseText}>Reverse Text</button>
           
        </div>
        <div className="container my-3">
            <h2>Your text summary is</h2>
            <p>{text.split(" ").length} words and {text.length} characters</p>
            <p>{0.008 * text.split(" ").length} minutes read</p>
            <h2>Preview</h2>
            <p>{text}</p>
        </div>
        </>
    )
}
